def translate(string)
  string.split.map! {|word| piggify(word)}.join(" ")
end


def piggify(word)

  vowels = ["a", "e", "i", "o", "u"]

  word.chars.each_with_index do |ltr, idx|
    if ltr == "q" && word[idx+1] == "u"
      return word[idx+2..-1] + word[0..idx+1] + "ay"
    elsif vowels.include?(ltr)
      return word[idx..-1] + word[0...idx] + "ay"
    end
  end
  word + "ay"
end
