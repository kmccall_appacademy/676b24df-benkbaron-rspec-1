def echo(string)
  string
end

def shout(string)
  string.upcase
end

def repeat(string, num_repeats=2)
  new_str = []
  num_repeats.times {new_str << string}
  new_str.join(" ")
end

def start_of_word(string, num_ltrs)
  string[0...num_ltrs]
end

def first_word(string)
  string.split.first
end

def titleize(string)
  little_words = ["and", "over", "the", "of", "a"]

  str_arr = string.split

  str_arr.map!.with_index do |el, idx|
    if idx == 0
      el.capitalize
    else
      little_words.include?(el) ? el : el.capitalize
    end
  end
  str_arr.join(" ")
end
