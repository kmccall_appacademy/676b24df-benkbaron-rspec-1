def add(x, y)
  x + y
end

def subtract(x, y)
  x - y
end

def sum(arr)
  arr.reduce(0) {|acc, el| acc += el}
end
